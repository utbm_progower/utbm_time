function dynamicColors() {
  var r = Math.floor(Math.random() * 255);
  var g = Math.floor(Math.random() * 255);
  var b = Math.floor(Math.random() * 255);
  return ["rgba(" + r + "," + g + "," + b + ", 0.2)", "rgba(" + r + "," + g + "," + b + ", 1)"];
}

function poolColors(size) {
  var pool_a = [];
  var pool_b = [];
  for(i = 0; i < size; i++) {
    var tmp = dynamicColors();
    pool_a.push(tmp[0]);
    pool_b.push(tmp[1]);
  }
  return [pool_a, pool_b];
}

function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  } : null;
}

function autoTextColor(background, colorBright = '#FFFFFF', colorDark = '#000000', seuil = 128) {
  rgb = hexToRgb(background);

  coef = rgb.r * 0.3 + rgb.g * 0.59 + rgb.b * 0.11;

  if (coef <= seuil)
    return colorBright;
  else
    return colorDark;
}
