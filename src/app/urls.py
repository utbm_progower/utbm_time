"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""


from django.contrib import admin
from django.urls import include, path
from django.conf import settings

from users import views as usersViews
from tasks import views as tasksViews


urlpatterns = [
    path('admin/', admin.site.urls, name='admin'),
    path('advanced_filters/', include('advanced_filters.urls')),

    path('', tasksViews.calendar, name='index'),
    path('import', tasksViews.importation, name='import'),

    path('login', usersViews.login, name='login'),
    path('register', usersViews.register, name='register'),
    path('setting', usersViews.setting, name='setting'),
    path('logout', usersViews.logout, name='logout')
]


if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
