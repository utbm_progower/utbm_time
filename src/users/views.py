from django.shortcuts import render, redirect

from django.contrib.auth import authenticate, login as login_system, logout as logout_system
from django.contrib.auth.decorators import login_required

from django.contrib.auth.models import User

from .forms import LoginForm
from .forms import RegisterForm
from .forms import SettingForm


@login_required
def index(request):
    return render(request, 'index.html')


def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(
                request,
                username=form.cleaned_data.get('username'),
                password=form.cleaned_data.get('password')
            )
            if user is not None:
                login_system(request, user)
                return redirect('index')
            else:
                form.add_error(None, 'Bad Login !')
        else:
            form.add_error(None, 'Bad Values !')
    else:
        form = LoginForm()
    return render(request, 'login.html', {'form': form})


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            if form.cleaned_data.get('password') == form.cleaned_data.get('password_check'):
                User.objects.create_user(
                    username=form.cleaned_data.get('username'),
                    password=form.cleaned_data.get('password'),
                    first_name=form.cleaned_data.get('first_name'),
                    last_name=form.cleaned_data.get('last_name'),
                    email=form.cleaned_data.get('email')
                )
                return redirect('login')
            else:
                form.add_error(None, "Password don't match !")
        else:
            form.add_error(None, 'Bad Values !')
    else:
        form = RegisterForm()
    return render(request, 'register.html', {'form': form})


@login_required
def setting(request):
    if request.method == 'POST':
        form = SettingForm(request.POST)
        if form.is_valid():
            if form.cleaned_data.get('password') == form.cleaned_data.get('password_check'):
                request.user.set_password(raw_password=form.cleaned_data.get('password'))
                request.user.save()
                return redirect('index')
            else:
                form.add_error(None, "Password don't match !")
        else:
            form.add_error(None, 'Bad Values !')
    else:
        form = SettingForm()
    return render(request, 'setting.html', {'form': form})


@login_required
def logout(request):
    logout_system(request)
    return redirect('login')
