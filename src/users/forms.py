from django import forms


class LoginForm(forms.Form):
    '''
    Form for Login User.
    '''
    username = forms.CharField(label='Username', max_length=190, help_text='Enter your username')
    password = forms.CharField(label='Password', help_text='Enter your password')


class RegisterForm(forms.Form):
    '''
    Form for Register User.
    '''
    username = forms.CharField(label='Username', max_length=190, help_text='Enter your username')

    first_name = forms.CharField(label='First Name', max_length=30, help_text='Enter your first name')
    last_name = forms.CharField(label='Last Name', max_length=150, help_text='Enter your last name')
    email = forms.EmailField(label='Email', help_text='Enter your email')

    password = forms.CharField(label='Password', help_text='Enter your password')
    password_check = forms.CharField(label='Password Verify', help_text='ReEnter your password')


class SettingForm(forms.Form):
    '''
    Form for update User Password
    '''
    password = forms.CharField(label='Password', help_text='Enter your password')
    password_check = forms.CharField(label='Password Verify', help_text='ReEnter your password')
