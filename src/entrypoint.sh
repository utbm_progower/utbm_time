#!/bin/sh

set -e

if [ "$DATABASE_ENGINE" = "django.db.backends.postgresql" ]
then
    echo "Waiting for PostGreSQL ..."

    while ! nc -z $DATABASE_HOST $DATABASE_PORT; do
      sleep 0.1
    done

    echo "PostGreSQL started"
fi

python manage.py migrate

exec "$@"
