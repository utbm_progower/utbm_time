from django.db import models

from datetime import timedelta

from django.contrib.auth.models import User


class Task(models.Model):
    """
    Task Object
    """

    id = models.AutoField(primary_key=True)
    uv = models.CharField(max_length=10, null=False)
    group = models.CharField(max_length=5, null=True)
    day = models.CharField(max_length=20, null=True)
    start = models.DateTimeField(null=False)
    stop = models.DateTimeField(null=False)
    frequency = models.IntegerField(null=True)
    mode = models.CharField(max_length=20, null=True)
    room = models.CharField(max_length=20, null=True)

    users = models.ManyToManyField(User, through='UserTask')

    def __str__(self):
        return "{} : {} | Group = {} | Day = {} | Start = {} | Stop = {} | Room = {}".format(
            self.id,
            self.uv,
            self.group,
            self.day,
            self.start,
            self.stop,
            self.room
        )


class UserTask(models.Model):
    """
    User Task Attribution
    """

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    week_group = models.CharField(max_length=5, null=True)
    is_active = models.BooleanField(null=True)

    def __str__(self):
        return "{} have {} | Week Group = {} | Active = {}".format(
            self.user,
            self.task,
            self.week_group,
            self.is_active
        )


class TaskColor(models.Model):
    """
    Task Color Settings
    """

    id = models.AutoField(primary_key=True)
    uv = models.CharField(max_length=10, null=False)
    color = models.CharField(max_length=8, null=False)

    def __str__(self):
        return "{} : {} = {}".format(
            self.id,
            self.uv,
            self.color
        )


def getCurrentDayList(date):
    """
    Return List of day of current week
    """
    week_day_number = date.weekday()
    if week_day_number == 0:
        week_day = date
    else:
        week_day = date - timedelta(days=week_day_number)

    day_list = {}

    day = 0
    for key in FRENCH_DAY_TO_ENGLISH_DAY:
        day_list[FRENCH_DAY_TO_ENGLISH_DAY[key]] = week_day + timedelta(days=day)
        day += 1

    return day_list


def getWeekGroup(date):
    """
    Return Week Group of current week
    """
    week_group = None

    week_day_number = date.weekday()
    if week_day_number == 0:
        week_day = date
    else:
        week_day = date - timedelta(days=week_day_number)

    if week_day.day == 13 and week_day.month == 9 and week_day.year == 2021:
        week_group = 'A'
    if week_day.day == 20 and week_day.month == 9 and week_day.year == 2021:
        week_group = 'B'
    if week_day.day == 27 and week_day.month == 9 and week_day.year == 2021:
        week_group = 'A'
    if week_day.day == 4 and week_day.month == 10 and week_day.year == 2021:
        week_group = 'B'
    if week_day.day == 11 and week_day.month == 10 and week_day.year == 2021:
        week_group = 'A'
    if week_day.day == 18 and week_day.month == 10 and week_day.year == 2021:
        week_group = 'B'
    if week_day.day == 25 and week_day.month == 10 and week_day.year == 2021:
        week_group = 'A'
    if week_day.day == 8 and week_day.month == 11 and week_day.year == 2021:
        week_group = 'B'
    if week_day.day == 15 and week_day.month == 11 and week_day.year == 2021:
        week_group = 'A'
    if week_day.day == 22 and week_day.month == 11 and week_day.year == 2021:
        week_group = 'B'
    if week_day.day == 29 and week_day.month == 11 and week_day.year == 2021:
        week_group = 'A'
    if week_day.day == 6 and week_day.month == 12 and week_day.year == 2021:
        week_group = 'B'
    if week_day.day == 13 and week_day.month == 12 and week_day.year == 2021:
        week_group = 'A'
    if week_day.day == 3 and week_day.month == 1 and week_day.year == 2022:
        week_group = 'B'

    if week_group is None:
        if week_day.isocalendar()[0] / 2 == 0:
            week_group = 'B'
        else:
            week_group = 'A'

    return week_group


DAYS = {
    '1': 'Monday',
    '2': 'Tuesday',
    '3': 'Wednesday',
    '4': 'Thursday',
    '5': 'Friday',
    '6': 'Saturday',
}


FRENCH_DAY_TO_ENGLISH_DAY = {
    'lundi': 'monday',
    'mardi': 'tuesday',
    'mercredi': 'wednesday',
    'jeudi': 'thursday',
    'vendredi': 'friday',
    'samedi': 'saturday',
    'dimanche': 'sunday',
}


IMPORT_EXAMPLE = """SV53 		TD 2 	lundi 	8:00 	9:45 	1 	Présentiel 	B 421
TA72 		CM 1 	lundi 	10:15 	12:00 	1 	Présentiel 	B 421
DA54 		CM 1 	mardi 	10:15 	12:00 	1 	Distanciel 	None
AI53 		CM 1 	mercredi 	8:00 	9:45 	1 	Présentiel 	I 102
LJ00 		TP 1 	mercredi 	13:00 	14:00 	1 	Présentiel 	B 421
LJ00 		TD 1 	mercredi 	14:30 	16:15 	1 	Présentiel 	B 421
AI53 		TD 2 	mercredi 	16:45 	18:30 	1 	Présentiel 	B 424
SV53 		CM 1 	jeudi 	8:00 	9:45 	1 	Présentiel 	B 424
AI53 		TP 2 	jeudi 	10:15 	13:00 	2 	Présentiel 	B 402
SV53 		TP 1 	jeudi 	10:15 	13:00 	2 	Présentiel 	H 010
TA72 		CM 1 	vendredi 	8:00 	9:45 	1 	Présentiel 	B 421
DA52 		CM 1 	vendredi 	13:00 	14:45 	1 	Présentiel 	I 102
DA52 		TD 1 	vendredi 	15:15 	17:00 	1 	Présentiel 	B 410
DA54 		TP 4 	samedi 	10:15 	13:00 	1 	Présentiel 	B 410"""
