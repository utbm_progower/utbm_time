from datetime import datetime, timedelta

from django.shortcuts import render
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.template.defaulttags import register

from .models import User
from .models import Task
from .models import UserTask
from .models import TaskColor
from .models import getCurrentDayList
from .models import getWeekGroup
from .models import DAYS
from .models import IMPORT_EXAMPLE
from .models import FRENCH_DAY_TO_ENGLISH_DAY


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


@login_required
def calendar(request):
    if request.GET:
        view = request.GET.get('view')
        calendar_height = request.GET.get('calendar_height')
        current_date = datetime.strptime(
            request.GET.get('current_date'),
            '%d/%m/%Y'
        )
        print(current_date)
        week_group = getWeekGroup(current_date)
        usersFilter = request.GET.getlist('users[]')
        daysFilter = request.GET.getlist('days[]')
        tmp_tasks = UserTask.objects.filter(
            Q(user__username__in=usersFilter) & Q(
                is_active=True) & (Q(
                    week_group=week_group) | Q(
                        week_group=None) | Q(
                            week_group='None') | Q(
                                week_group=''))
        )
    else:
        view = "week"
        calendar_height = "800"
        current_date = datetime.today()
        week_group = getWeekGroup(current_date)
        usersFilter = [request.user.username]
        daysFilter = [id for id, day in DAYS.items()]
        tmp_tasks = UserTask.objects.filter(
            Q(user=request.user) & Q(
                is_active=True) & (Q(
                    week_group=week_group) | Q(
                        week_group=None) | Q(
                            week_group='None') | Q(
                                week_group=''))
        )

    # Prepare Tasks List
    users = User.objects.all().order_by("last_name", "first_name")
    day_list = getCurrentDayList(current_date)
    tasks = {}
    task_color = {}

    for task in tmp_tasks:
        if task.task.id not in tasks:
            if task.task.day is not None:
                start_date = day_list.get(FRENCH_DAY_TO_ENGLISH_DAY.get(task.task.day))
                start_date = start_date.replace(hour=task.task.start.hour, minute=task.task.start.minute)
                stop_date = day_list.get(FRENCH_DAY_TO_ENGLISH_DAY.get(task.task.day))
                stop_date = stop_date.replace(hour=task.task.stop.hour, minute=task.task.stop.minute)

            tasks[task.task.id] = {
                "uv": task.task.uv,
                "group": task.task.group,
                "day": task.task.day,
                "start": start_date,
                "stop": stop_date,
                "frequency": task.task.frequency,
                "mode": task.task.mode,
                "room": task.task.room,
                "users": {"{} {}".format(task.user.first_name, task.user.last_name): True}
            }
        else:
            tasks[task.task.id]["users"]["{} {}".format(task.user.first_name, task.user.last_name)] = True

    task_color_tmp = TaskColor.objects.all()
    for color in task_color_tmp:
        if len(color.color) == 7:
            if color.uv not in task_color:
                task_color[color.uv] = color.color

    return render(request, 'calendar.html', {
        'view': view,
        'days': DAYS,
        'daysFilter': daysFilter,
        'previous_week': current_date - timedelta(days=7),
        'next_week': current_date + timedelta(days=7),
        'calendar_height': calendar_height,
        'current_date': current_date,
        'week_group': week_group,
        'users': users,
        'usersFilter': usersFilter,
        'tasks': tasks,
        'day_list': day_list,
        'task_color': task_color,
    })


@login_required
def importation(request):
    msgs = []
    if request.POST and request.POST.get('import') is not None:
        # Convert Data to Tasks
        import_tasks = request.POST.get('import')
        import_tasks = import_tasks.replace(" 		", ",")
        import_tasks = import_tasks.replace(" 	", ",")
        import_tasks = import_tasks.replace(" ", "")
        import_tasks = import_tasks.replace("Présentiel", "Presentiel")
        import_tasks = import_tasks.replace("\r", "")
        import_tasks = import_tasks.split("\n")
        import_tasks = [item for item in import_tasks if item != ""]

        for import_task in import_tasks:
            import_task = import_task.split(',')
            if len(import_task) == 8:
                task, update = Task.objects.update_or_create(
                    uv=import_task[0],
                    group=import_task[1],
                    day=import_task[2],
                    start=datetime.strptime(import_task[3], '%H:%M'),
                    stop=datetime.strptime(import_task[4], '%H:%M'),
                    frequency=int(import_task[5]),
                    mode=import_task[6],
                    room=import_task[7]
                )
                if request.POST.get('active') is not None:
                    # Add Tasks to User if not added
                    check = UserTask.objects.filter(
                        user=request.user,
                        task=task.id
                    ).first()
                    if check is None:
                        UserTask.objects.create(
                            user=request.user,
                            task=task,
                            week_group=None,
                            is_active=True
                        )
            else:
                msgs.append("Bad Value for Task ! (Value = {}) = {}".format(len(import_task), import_task))

    tasks = UserTask.objects.filter(user=request.user).order_by('task__day')

    return render(request, 'import.html', {
        'tasks': tasks,
        'import_example': IMPORT_EXAMPLE,
        'msgs': msgs,
    })
