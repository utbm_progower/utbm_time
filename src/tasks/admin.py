from django.contrib import admin
from advanced_filters.admin import AdminAdvancedFiltersMixin
from advanced_filters import forms as adv_forms

from .models import Task
from .models import UserTask
from .models import TaskColor

from .forms import TaskForm
from .forms import UserTaskForm


class MyAdvancedFilterForm(adv_forms.AdvancedFilterForm):
    def clean(self):
        for form in self.fields_formset:
            if not form.is_valid():
                form.cleaned_data = {'field': 'id', 'DELETE': True}
        return super().clean()


class MyAdminAdvancedFiltersMixin(AdminAdvancedFiltersMixin):
    advanced_filter_form = MyAdvancedFilterForm


@admin.register(Task)
class TaskAdmin(MyAdminAdvancedFiltersMixin, admin.ModelAdmin):
    list_filter = ['mode']
    search_fields = ['uv', 'day']
    empty_value_display = 'Empty'
    advanced_filter_fields = [
        'uv',
        'group',
        'day',
        'start',
        'stop',
        'frequency',
        'mode',
        'room'
    ]
    form = TaskForm


@admin.register(UserTask)
class UserTaskAdmin(AdminAdvancedFiltersMixin, admin.ModelAdmin):
    list_filter = ['week_group', 'is_active']
    search_fields = ['task__uv']
    empty_value_display = 'Empty'
    advanced_filter_fields = [
        'task__uv',
        'task__group',
        'task__day',
        'task__start',
        'task__stop',
        'task__frequency',
        'task__mode',
        'task__room',
        'week_group',
        'is_active'
    ]
    form = UserTaskForm


@admin.register(TaskColor)
class TaskColorAdmin(AdminAdvancedFiltersMixin, admin.ModelAdmin):
    search_fields = ['uv', 'color']
