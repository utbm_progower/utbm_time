from django import forms


class TaskForm(forms.ModelForm):
    DAY_CHOICES = (
        ('lundi', 'Lundi'),
        ('mardi', 'Mardi'),
        ('mercredi', 'Mercredi'),
        ('jeudi', 'Jeudi'),
        ('vendredi', 'Vendredi'),
        ('samedi', 'Samedi'),
        (None, 'None'),
    )
    MODE_CHOICES = (
        ('Presentiel', 'Présentiel'),
        ('Distanciel', 'Distanciel'),
    )

    group = forms.CharField(max_length=5, required=False)
    day = forms.ChoiceField(choices=DAY_CHOICES, required=False)
    frequency = forms.IntegerField(required=False)
    mode = forms.ChoiceField(choices=MODE_CHOICES)
    room = forms.CharField(max_length=20, required=False)


class UserTaskForm(forms.ModelForm):
    WEEK_GROUP_CHOICES = (
        ('A', 'A'),
        ('B', 'B'),
        (None, 'None'),
    )

    week_group = forms.ChoiceField(choices=WEEK_GROUP_CHOICES, required=False)
