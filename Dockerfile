# Builder
FROM python:3.8-slim as builder

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install --no-cache-dir --upgrade pip

COPY ./src/requirements.txt /app/requirements.txt

RUN pip wheel --no-cache-dir --no-deps --wheel-dir /app/wheels -r /app/requirements.txt


# Final
FROM python:3.8-slim

ENV DEBUG 0
ENV ENV production

RUN apt-get update && apt-get install -y netcat

COPY --from=builder /app/wheels /wheels

RUN pip install --no-cache /wheels/*

COPY ./src /app

WORKDIR /app

RUN chmod 777 -R *

RUN python manage.py check --deploy

EXPOSE 80

ENTRYPOINT ["/app/entrypoint.sh"]

CMD ["gunicorn", "app.asgi:application", "-k", "uvicorn.workers.UvicornWorker", "--bind", "0.0.0.0:80"]
