# UTBM Time

[![Master Status](https://gitlab.com/utbm_progower/utbm_time/badges/master/pipeline.svg)](https://gitlab.com/utbm_progower/utbm_time/-/commits/master)

[![Release Status](https://gitlab.com/utbm_progower/utbm_time/badges/release/pipeline.svg)](https://gitlab.com/utbm_progower/utbm_time/-/commits/release)

![Icon](./icon.png)

## Table Of Contents

- [UTBM Time](#utbm-time)
  - [Table Of Contents](#table-of-contents)
  - [Description](#description)
  - [Access](#access)
  - [Getting Started](#getting-started)
  - [Documentation](#documentation)
  - [Build](#build)
    - [Development Build : Docker](#development-build--docker)
    - [Development Build : Pipenv](#development-build--pipenv)
    - [Production Build](#production-build)
  - [Deploy](#deploy)
    - [Development Deployment : Docker](#development-deployment--docker)
    - [Development Deployment : Pipenv](#development-deployment--pipenv)
    - [Production Deployment](#production-deployment)
  - [Licence](#licence)

## Description

**UTBM Time** is a simple web application to convert list UTBM planning to calendar planning.

## Access

- Access in **Local Development** :
  - [UTBM Time](http://localhost:8007)
- Access in **Local Production** :
  - [UTBM Time](http://localhost:8007)
- Access in **Heroku Production** :
  - [UTBM Time](https://utbm-time.herokuapp.com/)

## Getting Started

Docker Compose file : **docker-compose.yml**

    version: '3.6'

    networks:
      utbm_time_net:

    volumes:
      utbm_time_db_data:

    services:
      utbm_time:
        container_name: utbm_time
        image: utbm_time:latest
        environment:
          - ENV=production
          - DEBUG=0
          - SECRET_KEY=your_secret_key
          - DJANGO_ALLOWED_HOSTS=localhost 127.0.0.1 [::1] 0.0.0.0 *
          - DATABASE_ENGINE=django.db.backends.postgresql
          - DATABASE_HOST=utbm_time_db
          - DATABASE_PORT=5432
          - DATABASE_DB=utbm_time
          - DATABASE_USER=admin
          - DATABASE_PASSWORD=Admin0!
        networks:
          - utbm_time_net
        restart: unless-stopped

      utbm_time_proxy:
        container_name: utbm_time_proxy
        image: utbm_time_proxy:latest
        networks:
          - utbm_time_net
        ports:
          - 8007:80
        restart: unless-stopped

      utbm_time_db:
        container_name: utbm_time_db
        image: postgres:latest
        environment:
          - POSTGRES_DB=utbm_time
          - POSTGRES_USER=admin
          - POSTGRES_PASSWORD=Admin0!
        volumes:
          - utbm_time_db_data:/var/lib/postgresql/data
        networks:
          - utbm_time_net
        restart: unless-stopped

Finaly you can start it, with these [command](#production-deployment).

**Environment Variables Definition** :

| Parameters | Value Example | Description |
|-|-|-|
| SECRET_KEY | 6*vwtzc30s0r5*7m$$oyd=oh#* | Secret Key for Password |
| DATABASE_USER and POSTGRES_USER | admin | User for Database Access |
| DATABASE_PASSWORD and POSTGRES_PASSWORD | Admin0! | User Password for Database Access |
|  |  |  |

For Generate a new **Secret Key** :

With **Python** and **Django** :

    from django.core.management.utils import get_random_secret_key
    get_random_secret_key()

You can also use this [website](https://djecrety.ir/) for generate a Secret Key.

For create a new Admin User : see it in the [Command Documentation](./docs/command.md)

## Documentation

Here some documentation :

- [Conception](./docs/conception.md)
- [Idea](./docs/idea.md)
- [Command](./docs/command.md)
- [Database](./docs/database.md)

## Build

### Development Build : Docker

    docker-compose -f docker-compose_dev.yml build

### Development Build : Pipenv

    cd src
    pipenv install

### Production Build

    docker-compose build

## Deploy

### Development Deployment : Docker

    # Start
    docker-compose -f docker-compose_dev.yml up -d

    # Stop
    docker-compose -f docker-compose_dev.yml down

### Development Deployment : Pipenv

    cd src
    pipenv run python manage.py runserver 0.0.0.0:8007

### Production Deployment

    # Start
    docker-compose up -d

    # Stop
    docker-compose down

## Licence

This project is licensed under the terms of the GNU General Public Licence v3.0 and above licence.
