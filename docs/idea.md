# UTBM Time : Idea

![Icon](../icon.png)

## Table Of Contents

- [UTBM Time : Idea](#utbm-time--idea)
  - [Table Of Contents](#table-of-contents)
  - [Upgrade Idea](#upgrade-idea)
  - [New Function Idea](#new-function-idea)
  - [Optimize Idea](#optimize-idea)

## Upgrade Idea

- Nothing.

## New Function Idea

- Nothing.

## Optimize Idea

- Nothing.
