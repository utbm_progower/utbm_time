# UTBM Time : Database

![Icon](../icon.png)

## Table Of Contents

- [UTBM Time : Database](#utbm-time--database)
  - [Table Of Contents](#table-of-contents)
  - [Database](#database)
  - [Database Schema](#database-schema)

## Database

**Database Name** : utbm_time

**Table List** :

- **users** : Users Management
- **tasks** : Tasks Management
- **users_tasks** : Users Tasks

## Database Schema

![Database](./img/database.png)
