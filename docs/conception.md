# UTBM Time : Conception

![Icon](../icon.png)

## Table Of Contents

- [UTBM Time : Conception](#utbm-time--conception)
  - [Table Of Contents](#table-of-contents)
  - [Objective](#objective)
  - [Leveling Analysis](#leveling-analysis)
    - [Level 001](#level-001)
    - [Level 002](#level-002)
  - [List of Main Feature](#list-of-main-feature)
  - [List of Secondary Feature](#list-of-secondary-feature)
  - [List of Optional Feature](#list-of-optional-feature)

## Objective

- Transform UTBM basic planning to an advanced planning.
- Manage tasks
- Manage Multi User planning

## Leveling Analysis

### Level 001

- Users Management
- Tasks Management
- Users Task Management

### Level 002

- **Users Management** :
  - Register
  - Login
  - Logout
  - Settings
- **Tasks Management** :
  - Get All
  - View Calendar (Single or Multi User)
  - Create (Admin ONLY)
  - Update (Admin ONLY)
  - Delete (Admin ONLY)
  - Create from UTBM (Copy Paste Update)
  - Convert UTBM Copy Paste to Tasks
- **Users Task Management** :
  - Add to User
  - Remove from User
  - Update to User from UTBM (Copy Paste Update)

## List of Main Feature

- **Users Management** :
  - Register
  - Login
  - Logout
  - Settings
- **Tasks Management** :
  - Get All
  - View Calendar (Single or Multi User)
  - Create (Admin ONLY)
  - Update (Admin ONLY)
  - Delete (Admin ONLY)
  - Create from UTBM (Copy Paste Update)
  - Convert UTBM Copy Paste to Tasks
- **Users Task Management** :
  - Add to User
  - Remove from User
  - Update to User from UTBM (Copy Paste Update)

## List of Secondary Feature

- **Tasks Management** :
  - Create (Admin ONLY)
  - Update (Admin ONLY)
  - Delete (Admin ONLY)

## List of Optional Feature

- Nothing
