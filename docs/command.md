# UTBM Time : Command

![Icon](../icon.png)

## Table Of Contents

- [UTBM Time : Command](#utbm-time--command)
  - [Table Of Contents](#table-of-contents)
  - [Local Init](#local-init)
  - [Command](#command)

## Local Init

    # From Scratch
    docker-compose -f ./docker-compose_init.yml run --rm utbm_time_init sh
    # Next use command for setup blank project

    # With Starter Kit
    docker-compose -f ./docker-compose_dev.yml run --rm utbm_time_dev sh

## Command

    # Init Project
    bash
    cd app

    # Pipenv Setup
    pipenv install -r requirements.txt
    pipenv shell
    
    # Install Dependencies
    pip install --no-cache-dir -r /app/requirements.txt

    # Setup a Django Project
    django-admin startproject app

    # Create new Application of Project
    python manage.py startapp users
    python manage.py startapp tasks

    # Add in setting.py at INSTALLED_APPS : users.apps.UsersConfig
    # Add in setting.py at INSTALLED_APPS : tasks.apps.TasksConfig

    # Database Init
    python manage.py migrate

    # Database Administrator
    python manage.py createsuperuser

    # Username : root
    # Password : root

    # Create Database Update
    python manage.py makemigrations

    # Apply Database Update
    python manage.py migrate

    # Fix Right Problem
    chmod 777 -R *

    # Run the server
    python manage.py runserver 0.0.0.0:80

    # Run Unit Test
    python manage.py test

    # Check Synthax
    flake8

    # Check Deployment
    python manage.py check --deploy

    # Collect Static Files
    python manage.py collectstatic
