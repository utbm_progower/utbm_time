#!/bin/bash


# Git Setup Multiple Repository
git remote add gitlab git@gitlab.com:utbm_progower/utbm_time.git
git remote set-url --add --push origin git@gitlab.com:utbm_progower/utbm_time.git

git remote add github git@github.com:UTBM-ProgOwer/UTBM_Time.git
git remote set-url --add --push origin git@github.com:UTBM-ProgOwer/UTBM_Time.git


# Display Config
git remote show origin

git config --list
